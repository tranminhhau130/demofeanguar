import { ErrormessageComponent } from './errormessage/errormessage.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [ErrormessageComponent],
  imports: [
    CommonModule,
  ],
  exports:[ErrormessageComponent]
})
export class ComponentsModule { }
