import { FormControl } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-errormessage',
  templateUrl: './errormessage.component.html',
  styleUrls: ['./errormessage.component.scss'],
})
export class ErrormessageComponent implements OnInit {
  @Input() name: string;
  @Input() validator: FormControl;
  message: any = {
    taiKhoan: {
      required: 'Tai Khoan Khong Duoc Bo Trong',
    },
    matKhau: {
      required: 'Tai Khoan Khong Duoc Bo Trong',
      minlength: 'Mat Khau It Nhat 3 ki tu',
      maxlength: 'Mat Khau NHieu Nhat 20 ki tu',
    },
    email: {
      required: 'Email Khong Duoc Bo Trong',
      pattern: 'Email khong dung dinh dang',
    },
    sdt: {
      required: 'So dien thoai Khong Duoc Bo Trong',
    },
    hoTen: {
      required: 'Ho Ten Khong Duoc Bo Trong',
    },
  };
  constructor() {}

  ngOnInit(): void {}
}
