import { AuthService } from './core/service/auth.service';
import { Component, OnInit } from '@angular/core';
// meta-data
@Component({
  selector: 'app-root', // Tên component để nhúng vào html
  templateUrl: './app.component.html', // Link tới file html
  styleUrls: ['./app.component.scss'], // Link tới file style
})
export class AppComponent {
  title = 'fe50-angular';
  constructor(private auth: AuthService) {}
  ngOnInit(): void {
    this.auth.initCurrentUser();
  }
}
