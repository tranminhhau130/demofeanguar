import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
// import vao app module
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const user = localStorage.getItem('user');
    if (user) {
      const { accessToken } = JSON.parse(user);
      // add authorization vao request header, gan lai request
      request = request.clone({
        headers: request.headers.append(
          'Authorizathion',
          `Bearer ${accessToken}`
        ),
      });
    }
    //gui lai request
    return next.handle(request);
  }
}
