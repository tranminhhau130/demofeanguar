import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  // doi tuong dung de luu tru 1 global value
  currentUser = new BehaviorSubject(null);
  // this.currentUser.next(value) set lai cho bien currentUser
  // this.currentUser.value: get gia tri cua CurrentUser, tai mot thoi diem
  //this.currentUser.asObervable chuyen Behaviour thanh mot Observable
  //thi co the subscribe de theo doi thay doi cua bien currentUser
  //this.currentUser.asObervable.subscribe theo doi thay doi cua bien currentUser
  constructor(private http: HttpClient) {}
  //kiem tra trong local co bien User khong
  initCurrentUser() {
    const user = localStorage.getItem('user');
    if (user) {
      this.currentUser.next(JSON.parse(user));
    }
  }
  signUp(value: any): Observable<any> {
    const url = 'https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy';
    return this.http.post(url, { ...value, maNhom: 'GP01' });
  }
  signIn(value: any): Observable<any> {
    const url =
      'https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap';
    return this.http.post(url, value);
  }
}
