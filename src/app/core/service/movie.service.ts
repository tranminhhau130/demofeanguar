import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Movie } from './../models/movie';
import { Injectable } from '@angular/core';
// import { resolve } from 'dns';
// import { rejects } from 'assert';
import {Observable, Subscriber} from "rxjs"

@Injectable({
  //// tu phien ban angular 5 tro len, khi  co providedIn:'root',
  // khong can phai inport service vao trong providers o app module
  providedIn: 'root'
})
export class MovieService {
  constructor(private http : HttpClient) {}
  // getMovieList(): Movie[]{
  //   return [
  //     {
  //       name: 'Avenger',
  //       price: 80000,
  //       image: 'assets/images/avenger.jpeg',
  //     },
  //     {
  //       name: 'Iron man',
  //       price: 80000,
  //       image: 'assets/images/iron-man.jpeg',
  //     },
  //     {
  //       name: 'One punch man',
  //       price: 80000,
  //       image: 'assets/images/one-punch-man.jpeg',
  //     },
  //   ];
  // }
  getMovieList(): Observable <Movie[]>{
    const url = "https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim";
    let params = new HttpParams();
    params = params.append('maNhom', 'GP01');
    return this.http.get<Movie[]>(url, {params})
  }
  getMovieDetail(id: string): Observable<any>{
    const url = "https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim"
    let params = new HttpParams();
    params = params.append('maPhim', id);
    return this.http.get(url, {params})
  }
  // getMovieListPromise(){
  //   return new Promise ((resolve, reject)=>{
  //     setTimeout(()=>{resolve(
  //       [
  //         {
  //           name: 'Avenger',
  //           price: 80000,
  //           image: 'assets/images/avenger.jpeg',
  //         },
  //         {
  //           name: 'Iron man',
  //           price: 80000,
  //           image: 'assets/images/iron-man.jpeg',
  //         },
  //         {
  //           name: 'One punch man',
  //           price: 80000,
  //           image: 'assets/images/one-punch-man.jpeg',
  //         },
  //       ]
  //     )},2000)
  //     // setTimeout(()=>{reject('Error')},2000)
  //   })
  // }
  // getMovieListObservable(){
  //   return new Observable (subscriber=>{
  //     setTimeout(()=>{
  //       // subcriber.next emit du lieu
  //       // se khong ket thuc observable
  //       subscriber.next(
  //         [
  //           {
  //             name: 'Avenger',
  //             price: 80000,
  //             image: 'assets/images/avenger.jpeg',
  //           },
  //         ]
  //       )
  //     },2000)


  //     setTimeout(()=>{
  //       // subcriber.next emit du lieu
  //       // se khong ket thuc observable
  //       subscriber.next(
  //         [
  //           {
  //             name: 'Iron man',
  //             price: 80000,
  //             image: 'assets/images/iron-man.jpeg',
  //           },
  //         ]
  //       )
  //             //ket thuc observable
  //       // subscriber.complete()
  //             //error: ket thuc observable
  //     subscriber.error('network error')
  //     },4000)
  //   })
  // }
}
