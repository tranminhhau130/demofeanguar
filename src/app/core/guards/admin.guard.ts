import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    // return true cho phep truy cap vao va nguoc lai
    const user = localStorage.getItem('user');
    if (user) {
      const { maLoaiNguoiDung } = JSON.parse(user);
      if (maLoaiNguoiDung === 'QuanTri') {
        // da dang nhap va co quyen quan tri => cho phep truy cap
        return true;
      }
      // da dang nhap khong co quyen admin => redirect ve home
      this.router.navigate(['/']);
      return false;
    }
    // chua dang nhap => redirect ve signin
    this.router.navigate(['/signin']);
    return false;
  }
}
