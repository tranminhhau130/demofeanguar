import { SignupComponent } from './../../auth/signup/signup.component';
import { Injectable } from '@angular/core';
import {
  CanDeactivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
// chuyen unknown sang component muon candeactive
export class SignupGuard implements CanDeactivate<SignupComponent> {
  constructor() {}
  canDeactivate(
    component: SignupComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    // return true cho phep ra khoi page
    const isDirty = component.isDirtyForm();
    if (isDirty) {
      return confirm('Ban co muon roi khoi');
    }
    return true;
  }
}
