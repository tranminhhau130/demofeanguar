import { MovieComponent } from './movie/movie.component';
import { BookingComponent } from './booking/booking.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';

const routes: Routes = [
  {path:"", component: MainComponent, children:[
    {path:"", component: HomeComponent},
      // {path:"movie/:maPhim", component:MovieComponent},
    {path:"movie", component:MovieComponent},
  ]},
  {path:"booking", component:BookingComponent},

];

@NgModule({
  // dung cho cac router con thi su dung forChild (app router dung forRoot)
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
