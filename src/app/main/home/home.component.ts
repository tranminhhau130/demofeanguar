import { Movie } from './../../core/models/movie';
import { MovieService } from './../../core/service/movie.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  movieList: Movie[] =[]
  // khai bao trong constructor moi co the su dung service
  constructor(private movieService:MovieService) { }


  // tuong duong component componentDidMount ben react
  //goi API, tuong tac DOM, setTimeout
  ngOnInit(): void {

    // httpClient
        this.movieService.getMovieList().subscribe(
      {
        //next: nhan ket qua
        next:(result)=>{
          this.movieList = result;
          console.log(this.movieList);
          
        },
        error:(error)=>{
          console.log(error);
        },
        complete:()=>{
          console.log("Done");
        }
      })

          //promise
    // this.movieService.getMovieListPromise()
    // .then(res=>{
    //   console.log(res);
      
    //   this.movieList = res as any
    // })
    // .catch(error=> console.log(error));


    // observable
    // this.movieService.getMovieListObservable().subscribe(
    //   {
    //     //next: nhan ket qua
    //     next:(result)=>{
    //       this.movieList = result as any
    //     },
    //     error:(error)=>{
    //       console.log(error);
    //     },
    //     complete:()=>{
    //       console.log("Done");
    //     }
    //   })

  }

}
