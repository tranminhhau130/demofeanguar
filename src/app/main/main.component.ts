import { AuthService } from './../core/service/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  currentUser: any;
  constructor(private auth: AuthService) {}

  ngOnInit(): void {
    // this.currentUser = this.auth.currentUser.value chi cap nhat duoc 1 lan
    this.auth.currentUser.asObservable().subscribe({
      next: (result) => {
        this.currentUser = result;
      },
    });
  }
}
