import { PipeModule } from './../share/pipe/pipe.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';

import { MainRoutingModule } from './main-routing.module';
import { MovieComponent } from './movie/movie.component';
import { BookingComponent } from './booking/booking.component';
import { HomeComponent } from './home/home.component';
import { MainComponent } from './main.component';


@NgModule({
  declarations: [MovieComponent, BookingComponent, HomeComponent, MainComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    PipeModule,
    MatButtonModule
  ],
  
})
export class MainModule { }
