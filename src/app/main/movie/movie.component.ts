
import { MovieService } from './../../core/service/movie.service';
import { Component, OnInit } from '@angular/core';
// tuong tac voi params
import {ActivatedRoute} from "@angular/router";

import {Subscription} from "rxjs"

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
  maPhim: any
  tenPhim:any
  movie:any = null
  loading: boolean = false
  error: string =""
  //demo
  obj: any = {
    hoTen : "hau",
    age : 25
  }
  constructor(private activatedRoute: ActivatedRoute, private movieService : MovieService) { }

  ngOnInit(): void {
    // this.activatedRoute.params.subscribe({
    //   next: (params)=>{
    //     this.loading =true;
    //     // console.log(params);
    //     console.log(this.loading);
    //     this.movieService.getMovieDetail(params.maPhim).subscribe({
    //       next: (result)=>{
    //         this.movie = result
    //         console.log(result);
    //         this.loading =false
    //       },
    //       error: error =>{
    //         console.log(error);
    //         this.loading =false
    //         this.error = error.error
    //       }
    //     })
    //   }
    // })

    this.activatedRoute.queryParams.subscribe({
      next:(params)=>{
        this.loading =true;
        this.movieService.getMovieDetail(params.maPhim).subscribe({
          next:(result)=>{
            this.movie = result
            this.loading= false
          },
          error: error =>{
          this.loading=false,
          this.error = error.error;
          
          }
        })
        
      }
    })
  }

}
