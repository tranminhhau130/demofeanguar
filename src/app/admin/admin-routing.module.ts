import { AdminComponent } from './admin.component';
import { MovieComponent } from './movie/movie.component';
import { UsersComponent } from './users/users.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:"", component: AdminComponent, children:[
    //chuyen path tu rong sang user (de set trang mac dinh)
    {path:"", redirectTo:'user'},
    {path:"user", component: UsersComponent},
    {path:"movie", component: MovieComponent}
  ]}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
