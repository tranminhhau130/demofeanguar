import { Router } from '@angular/router';
import { AuthService } from './../../core/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})
export class SigninComponent implements OnInit {
  signInForm: FormGroup;

  constructor(private authService: AuthService, private router: Router) {
    this.signInForm = new FormGroup({
      taiKhoan: new FormControl('', [Validators.required]),
      matKhau: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
      ]),
    });
  }

  ngOnInit(): void {}

  handleSignIn() {
    // danh dau cac form trang thai touched true
    this.signInForm.markAllAsTouched();
    if (this.signInForm.invalid) return;
    console.log(this.signInForm.value);
    this.authService.signIn(this.signInForm.value).subscribe({
      next: (result) => {
        console.log(result);
        localStorage.setItem('user', JSON.stringify(result));
        // cap nhat bien user vaof current User vao AuthService
        this.authService.currentUser.next(result);
        this.router.navigate(['./']);
      },
      error: (error) => {
        console.log(error.error);
      },
    });
  }
}
