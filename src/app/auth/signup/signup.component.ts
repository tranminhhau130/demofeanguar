import { AuthService } from './../../core/service/auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) {}
  @ViewChild('signUpForm') signUpForm: NgForm;
  ngOnInit(): void {}
  // kiem tra form da thay doi chua va chua submit
  isDirtyForm(): boolean {
    return this.signUpForm.dirty && !this.signUpForm.submitted;
  }
  handleSignUp(value) {
    console.log(value);
    this.authService.signUp(value).subscribe({
      next: (result) => {
        //chuyen trang
        this.router.navigate(['/signin']);
      },
      error: (err) => {
        console.log(err.error);
      },
    });
  }
}
