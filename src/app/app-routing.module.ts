import { AdminGuard } from './core/guards/admin.guard';
import { AuthModule } from './auth/auth.module';
// import { AdminModule } from './admin/admin.module';
// import { MainModule } from './main/main.module';
import { CardComponent } from './directives/card/card.component';
import { MovieComponent } from './movie/movie.component';
import { Baitap2Component } from './baitap2/baitap2.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // routing theo component
  // {path: "baitap2", component: Baitap2Component},
  // {path:"movie", component:MovieComponent},
  // {path:"card", component:CardComponent},
  // routing theo module
  // pathMacth:"full" la exact trong react
  // {path:"", pathMatch:"full" ,loadChildren:()=>MainModule}
  {
    path: 'admin',
    // truyen guard moi tao de bao ve trang muon su dung
    canActivate: [AdminGuard],
    loadChildren: () =>
      import('./admin/admin.module').then((m) => m.AdminModule),
  },
  {
    path: '',
    loadChildren: () => import('./main/main.module').then((m) => m.MainModule),
  },
  {
    path: '',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
